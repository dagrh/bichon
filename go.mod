module gitlab.com/bichon-project/bichon

go 1.12

require (
	github.com/gdamore/tcell v1.1.2
	github.com/go-ini/ini v1.42.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/rivo/tview v0.0.0-20190626133940-2e907d29e475
	github.com/spf13/pflag v1.0.3
	github.com/xanzy/go-gitlab v0.18.0
)
