Bichon Code Review
==================

Bichon aims to provide an excellent terminal based user interface for
reviewing GitLab merge requests, for developers who find web based user
interfaces unappealing or inefficient.

Some of the key goals

 - Enable all code review tasks to be fully accomplished via
   the keyboard, with appropriate shortcuts, with no need to
   switch to the web UI.

 - Local caching of merge requests to allow for offline code
   review with later publication at time of reconnect.

 - Customisable and extendable to allow developers to enhance
   the standard behaviour to suit their preferred workflow.

 - Familiar interaction model for developers who have traditionally
   done email based code review with the mutt email client.

Bichon is open source software made available under the terms of the
Apache 2.0 license.


Installation
============

Bichon is written in the Go programming language, making use of a number
of excellant 3rd party Go libraries to speed up its development.

It uses the modern Go module system, so does not have to be placed within
a specific $GOPATH diretory hierarchy, but as such requires Golang >= 1.12.

Simply run 'go build' from the top level directory which will create a
standardlone "bichon" binary


Configuration
=============

Bichon uses the GitLab REST API for it tasks and thus requires an API token
to be created.

 - Login to https://gitlab.com  (or your own hosted instance of GitLab)

 - Click your profile icon in top right and select "Settings"

 - Select "Access Tokens" from the left hand panel to display the
   form to add a new access token

 - Give the token any desired name (eg "bichon") and ensure the
   access scope selected is "api". Expiry date is optional.

 - Submit the form to create the access token and make a note of
   the token string as you can't retrieve it later

 - Create a file $HOME/.config/bichon/servers.conf containing

   ```
     [gitlab.com]
     token=...your access token string...
   ```

   The config file can have many access tokens, simply create new
   sections with the hostname of the custom hosted gitlab instance.

Now change the current directory to be within a git checkout of a project
hosted on gitlab, and simply start bichon.

It will examine .git/config to identify which gitlab server is set for
the "origin" remote and the name / namespace of the project.


History / Motivation
====================

Historically many large open source projects have used an email / mailing
list based code review workflow for a variety of reasons, including (but
not limited to):

 - Efficiency of an entirely keyboard based workflow with no
   need to use the mouse.

 - Use of open standards with no lockin to a vendor specific
   tool

 - Ability to have an arbitrary choice of client interface and
   built custom tools/workflows

 - Ability to self-host all the project infrastructure needed
   for the development workflow

Increasingly, however, newly launched projects will eschew email, picking
a web based git hosting service such as GitHub or GitLab. These lower the
barrier to starting a new project by having a single tool to cover all
common development tasks. The standardization of the project workflow and
tools across the ecosystem facilitates new contributors participating in
a project, whether for the long term, or a one-off drive-by submission.

As a result, projects employing email based code review worflows are
increasingly appear antiquated to outside observers. Although hard to
measure with accuracy, the perception is that these projects will not
attract contributions so easily.

The Bichon tool / project is an attempt to bridge the gap between the
historic email based code review workflows and modern web based git
hosting and code review tools.

The intent is that a project can adopt new hosted, web based code review
on GitLab, without entirely discarding the attractive parts of an email
based code review workflow for those developers who like that.

The mutt email client is widely used among developers involved in projects
using email based code review, which developed from ideas and features
found across a variety of earlier email clients. It is effective at
coping with the many tasks for which email is used.

Bichon is directly inspired by mutt, but aims to be a beautiful pure bred
client exclusively for the task of code review with GitLab merge requests.
