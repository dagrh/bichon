// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"fmt"
	"net/url"
	"os"
	"path"

	"github.com/go-ini/ini"
	"github.com/golang/glog"
)

type Repo struct {
	Directory string
	Server    string
	Project   string
}

func findGitDir(dir string) (string, error) {
	gitdir := path.Join(dir, ".git")
	_, err := os.Stat(gitdir)
	if err != nil {
		if !os.IsNotExist(err) {
			return "", err
		}
		parent, _ := path.Split(dir)
		return findGitDir(parent)
	}

	return gitdir, nil
}

func findGitOrigin(dir string) (string, string, error) {
	gitdir, err := findGitDir(dir)
	if err != nil {
		return "", "", err
	}

	gitcfg := path.Join(gitdir, "config")

	cfg, err := ini.Load(gitcfg)
	if err != nil {
		return "", "", err
	}

	origin := cfg.Section("remote \"origin\"").Key("url").String()

	if origin == "" {
		return "", "", fmt.Errorf("No origin found in %s", gitcfg)
	}

	return origin, dir, nil
}

func FindLocalRepo() (*Repo, error) {
	here, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	origin, gitdir, err := findGitOrigin(here)
	if err != nil {
		return nil, err
	}

	originurl, err := url.Parse(origin)
	if err != nil {
		return nil, err
	}

	repo := &Repo{
		Directory: gitdir,
		Server:    originurl.Host,
		Project:   originurl.Path[1:],
	}

	glog.Infof("Repo '%s' host '%s' project '%s'",
		repo.Directory, repo.Server, repo.Project)
	return repo, nil
}

func (repo *Repo) GetToken() (string, error) {
	home := os.Getenv("HOME")
	file := home + "/.config/bichon/servers.conf"

	cfg, err := ini.Load(file)
	if err != nil {
		return "", err
	}

	token := cfg.Section(repo.Server).Key("token").String()

	glog.Infof("Repo host '%s' token '%s'",
		repo.Server, token)

	if token == "" {
		return "", fmt.Errorf("No authentication in %s for server %s",
			file, repo.Server)
	}

	return token, nil
}
