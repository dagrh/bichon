// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"time"
)

type MergeReq struct {
	ID          uint
	Title       string
	CreatedAt   *time.Time
	UpdatedAt   *time.Time
	Submitter   Account
	Description string
}

func (mreq *MergeReq) Age() string {
	age := time.Since(*mreq.CreatedAt)
	return formatDuration(age)
}

func (mreq *MergeReq) Activity() string {
	age := time.Since(*mreq.UpdatedAt)
	return formatDuration(age)
}
