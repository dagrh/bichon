// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package source

import (
	"github.com/xanzy/go-gitlab"

	"gitlab.com/bichon-project/bichon/model"
)

type GitLab struct {
	Client *gitlab.Client
	Repo   *model.Repo
}

func NewGitLabForRepo(repo *model.Repo) (*GitLab, error) {
	token, err := repo.GetToken()
	if err != nil {
		return nil, err
	}

	client := gitlab.NewClient(nil, token)

	return &GitLab{
		Client: client,
		Repo:   repo,
	}, nil
}

func (gl *GitLab) Ping() error {
	_, _, err := gl.Client.Projects.GetProject(gl.Repo.Project, nil)
	if err != nil {
		return err
	}
	return nil
}

func (gl *GitLab) GetMergeRequests() ([]model.MergeReq, error) {
	state := "all"
	glmreqs, _, err := gl.Client.MergeRequests.ListProjectMergeRequests(
		gl.Repo.Project,
		&gitlab.ListProjectMergeRequestsOptions{
			State: &state,
		})
	if err != nil {
		return []model.MergeReq{}, err
	}

	mreqs := make([]model.MergeReq, 0)
	for _, glmreq := range glmreqs {
		mreq := model.MergeReq{
			ID:        uint(glmreq.IID),
			Title:     glmreq.Title,
			CreatedAt: glmreq.CreatedAt,
			UpdatedAt: glmreq.UpdatedAt,
			Submitter: model.Account{
				UserName: glmreq.Author.Username,
				RealName: glmreq.Author.Name,
			},
			Description: glmreq.Description,
		}

		mreqs = append(mreqs, mreq)
	}
	return mreqs, nil
}

func (gl *GitLab) GetVersions(mreq *model.MergeReq) ([]model.Series, error) {
	glvers, _, err := gl.Client.MergeRequests.GetMergeRequestDiffVersions(
		gl.Repo.Project, int(mreq.ID), nil,
	)
	if err != nil {
		return []model.Series{}, err
	}

	vers := make([]model.Series, 0)

	for idx, glver := range glvers {
		ver := model.Series{
			Index:   idx + 1,
			Version: glver.ID,
		}

		vers = append(vers, ver)
	}

	return vers, nil
}

func (gl *GitLab) GetPatches(mreq *model.MergeReq, series *model.Series) ([]model.Commit, error) {
     	glver, _, err := gl.Client.MergeRequests.GetSingleMergeRequestDiffVersion(
		gl.Repo.Project, int(mreq.ID), series.Version, nil,
	)
	if err != nil {
		return []model.Commit{}, err
	}

	commits := make([]model.Commit, 0)

	for _, glcommit := range glver.Commits {
		commit := model.Commit{
			Hash:  glcommit.ID,
			Title: glcommit.Title,
			Author: model.User{
				Name:  glcommit.AuthorName,
				Email: glcommit.AuthorEmail,
			},
			AuthorDate: glcommit.AuthoredDate,
			Committer: model.User{
				Name:  glcommit.CommitterName,
				Email: glcommit.CommitterEmail,
			},
			CommitterDate: glcommit.CommittedDate,
			Message:       glcommit.Message,
		}

		commits = append(commits, commit)
	}

	return commits, nil
}

func (gl *GitLab) GetMergeRequestComments(mreq *model.MergeReq) ([]model.Comment, error) {
	glnotes, _, err := gl.Client.Notes.ListMergeRequestNotes(
		gl.Repo.Project, int(mreq.ID), nil,
	)
	if err != nil {
		return []model.Comment{}, err
	}

	comments := make([]model.Comment, 0)
	for _, glnote := range glnotes {
		comment := model.Comment{
			Created: glnote.CreatedAt,
			Author: model.User{
				Name:  glnote.Author.Name,
				Email: glnote.Author.Email,
			},
			Description: glnote.Body,
		}

		comments = append(comments, comment)
	}

	return comments, nil
}
