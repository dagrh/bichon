// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strings"
	"time"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/source"
)

type DetailPage struct {
	Source   source.Source
	Messages Messages
	Layout   *tview.Flex
	Patches  *tview.Table
	Header   *tview.Frame
	Content  *tview.TextView
}

func NewDetailPage(source source.Source, msgs Messages) *DetailPage {
	patches := tview.NewTable().
		SetSelectable(true, false).
		SetSelectedStyle(tcell.ColorYellow, tcell.ColorRed, tcell.AttrBold)

	header := tview.NewFrame(patches).
		SetBorders(0, 0, 0, 0, 0, 0).
		AddText("[yellow:blue]"+strings.Repeat(" ", 500), false, tview.AlignLeft, tcell.ColorWhite)

	content := tview.NewTextView().
		SetDynamicColors(true)

	layout := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(header, 7, 1, false).
		AddItem(content, 0, 1, false)

	page := &DetailPage{
		Source:   source,
		Messages: msgs,
		Layout:   layout,
		Patches:  patches,
		Header:   header,
		Content:  content,
	}

	return page
}

func (page *DetailPage) GetName() string {
	return "detail"
}

func (page *DetailPage) GetKeyShortcuts() string {
	return "q:Quit r:Refresh ?:Help"
}

func (page *DetailPage) updatePatchesJob(app *tview.Application, mreq *model.MergeReq) {
	cells := make([][6]*tview.TableCell, 0)

	vers, err := page.Source.GetVersions(mreq)
	if err != nil {
		return
	}

	patches, err := page.Source.GetPatches(mreq, &vers[0])
	if err != nil {
		return
	}

	comments, err := page.Source.GetMergeRequestComments(mreq)
	if err != nil {
		return
	}

	cells = append(cells, [6]*tview.TableCell{
		&tview.TableCell{
			Text:          fmt.Sprintf("#%-4d", mreq.ID),
			Color:         tcell.ColorWhite,
			Align:         tview.AlignRight,
			NotSelectable: false,
		},
		&tview.TableCell{
			Text:          mreq.Age(),
			Color:         tcell.ColorWhite,
			Align:         tview.AlignLeft,
			NotSelectable: false,
		},
		&tview.TableCell{
			Text:          mreq.Submitter.RealName,
			Color:         tcell.ColorWhite,
			Align:         tview.AlignLeft,
			NotSelectable: false,
		},
		&tview.TableCell{
			Text:          fmt.Sprintf("v%d", len(vers)),
			Color:         tcell.ColorWhite,
			Align:         tview.AlignLeft,
			NotSelectable: false,
		},
		&tview.TableCell{
			Text:          fmt.Sprintf("0/%d", len(patches)),
			Color:         tcell.ColorWhite,
			Align:         tview.AlignLeft,
			NotSelectable: false,
		},
		&tview.TableCell{
			Text:          mreq.Title,
			Color:         tcell.ColorWhite,
			Align:         tview.AlignLeft,
			NotSelectable: false,
		},
	})
	for idx, patch := range patches {
		cells = append(cells, [6]*tview.TableCell{
			&tview.TableCell{
				Text:          "",
				Color:         tcell.ColorWhite,
				Align:         tview.AlignRight,
				NotSelectable: false,
			},
			&tview.TableCell{
				Text:          patch.Age(),
				Color:         tcell.ColorWhite,
				Align:         tview.AlignLeft,
				NotSelectable: false,
			},
			&tview.TableCell{
				Text:          mreq.Submitter.RealName,
				Color:         tcell.ColorWhite,
				Align:         tview.AlignLeft,
				NotSelectable: false,
			},
			&tview.TableCell{
				Text:          "",
				Color:         tcell.ColorWhite,
				Align:         tview.AlignLeft,
				NotSelectable: false,
			},
			&tview.TableCell{
				Text:          fmt.Sprintf("%d/%d", idx+1, len(patches)),
				Color:         tcell.ColorWhite,
				Align:         tview.AlignLeft,
				NotSelectable: false,
			},
			&tview.TableCell{
				Text:          patch.Title,
				Color:         tcell.ColorWhite,
				Align:         tview.AlignLeft,
				NotSelectable: false,
			},
		})
	}

	go func() {
		app.QueueUpdateDraw(func() {
			page.updatePatches(cells)
			page.updateCoverContent(mreq, comments)
			page.Messages.Info("")
		})
	}()
}

func (page *DetailPage) Refresh(app *tview.Application, mreq *model.MergeReq) {
	page.Content.SetText("")
	page.Messages.Info(fmt.Sprintf("Loading merge request %d patches...", mreq.ID))
	go page.updatePatchesJob(app, mreq)
}

func (page *DetailPage) SetFocus(app *tview.Application) {
	app.SetFocus(page.Content)
}

func (page *DetailPage) Navigate(event *tcell.EventKey) {
	if event.Key() == tcell.KeyLeft {
		row, col := page.Patches.GetSelection()
		if row > 0 {
			page.Patches.Select(row-1, col)
		}
	} else if event.Key() == tcell.KeyRight {
		row, col := page.Patches.GetSelection()
		if row < (page.Patches.GetRowCount() - 1) {
			page.Patches.Select(row+1, col)
		}
	}
}

func (page *DetailPage) updatePatches(cells [][6]*tview.TableCell) {
	page.Patches.Clear()
	for idx, row := range cells {
		page.Patches.SetCell(idx, 0, row[0])
		page.Patches.SetCell(idx, 1, row[1])
		page.Patches.SetCell(idx, 2, row[2])
		page.Patches.SetCell(idx, 3, row[3])
		page.Patches.SetCell(idx, 4, row[4])
		page.Patches.SetCell(idx, 5, row[5])
	}
}

func (page *DetailPage) updateCoverContent(mreq *model.MergeReq, comments []model.Comment) {
	body :=
		"[::b]Merge request: [red]" + tview.Escape(mreq.Title) +
			"[white] on [yellow]" + mreq.CreatedAt.Format(time.RFC1123) + "[-:-:-]\n\n" +
			tview.Escape(mreq.Description) + "\n\n\n"

	for idx, _ := range comments {
		comment := comments[len(comments)-1-idx]
		body = body +
			fmt.Sprintf("[::b]Comment #%d by [red]%s[white] on [yellow]%s[-:-:-]\n\n\n", idx+1,
				comment.Author.Name, comment.Created.Format(time.RFC1123)) +
			tview.Escape(comment.Description) + "\n\n\n"
	}

	page.Content.SetText(body)
}
